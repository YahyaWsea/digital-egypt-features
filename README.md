<h1  align="center"> Digital Egypt Project </h1>

## Table of Contents

- [Introduction](#introduction)

- [Tech Stack](#tech-stack)

- [Features](#features)

- [Challenges](#challenges)

- [Implemented Solutions](#implemented-solutions)

- [Requirements](#requirements)


- [Additional Resources](#additional-resources)

## Introduction

This article is intended to list some of digital egypt project features and challenges presented to [CobbleWeb](https://www.cobbleweb.co.uk/).

The Digital Egypt Project provides and manages government services, but in a digital form that benefits citizens and companies

## Tech Stack

- [Nodejs](https://nodejs.org/) combined with [TypeScript](https://www.typescriptlang.org) and [Expressjs](https://expressjs.com) as minimal web framework.
- [React.js](https://react.dev/) as the frontend library
- Database Used is [MongoDB](https://www.mongodb.com/) combined with [Mongoose](https://mongoosejs.com/) as ODM
- [RabbitMQ](https://www.rabbitmq.com/) as message broker
- [Elasticsearch](https://www.elastic.co/) as search engine
- [Monstache](https://rwynn.github.io/monstache-site/) for data sync from the persisted database to elasticsearch.
- [Redis](https://redis.io/) used for caching.

## Features

- The application provides unified identity solution for citizen that could be used by other applications or platforms to identify the citizen.

- Citizen can perform as many requests on any goverment services and follow up on the status of these requests through his own interface ***(Citizen Interface)***

- Citizens can pay for services they applying whether online or using POS method

- Employess from different entities and companies can take actions on the citizen requests presented from his own interface ***( Employee interface )***.

- About more than 20 million citizen uses our platfoms

- More than 30 million requests on different services are performed

- The Average requests performed or an action taken on it daily could be 5000 request


## Challenges

- ### Build a management interace for the following purposes:

1. configure any service flow dynamically without need to modify the codebase in case of any addition or modification on the government service workflow.

2. configure any kind of payment gateway in case it will be required to integrate with.

3. configure any kind of identity providers that could be asked to implement and use.

- ### launching ***(notification service)*** to handle any kind of notification sent from our main backend server ( named: middleware ) and finding proper way for the communication between them.

- ### capability to search through millions of records fast using different search parameters and extract reports and statistics from these records.


## Implemented Solutions

1. Design backend to accept dealing with different configured payment gateways that could be configured from the management interface

2. Using Identiy Management Solution (we use [Keycloak](https://www.keycloak.org/) ) to handle the authentication and authorization for our user models.

3. Using [RabbitMQ](https://www.rabbitmq.com/) for better communication between our bckend (middleware) and notification service to ensure that notifications are sent under any conditions ( eventual consistency ) ( see the below flow ).
![rabbitmq](docs/rabbitmq.png)

4. Using [Elasticsearch](https://www.elastic.co/) for the high performance and speed of retrieving and searching data of requests related to citizens or when we need to have reports related to info in these requests (see the below figure).
![elasticsearch-flow](docs/elasticsearch.png).


## Additional Resources

### Domains of the projects:
- [Digital Egypt](https://digital.gov.eg/)
- [Electricity Platform](https://eservices.eehc.gov.eg/)

### Important Note:
When I mention "user requests" that means the group of form data citizen submits in specified government service and employee could have action on this request to send request back to citizen to submit more data or changing status of request until the completion of request whether ( approving or rejecting ).

